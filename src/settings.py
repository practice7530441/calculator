import os
from functools import lru_cache

from pydantic_settings import BaseSettings

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
NAME_SERVICE_DEFAULT = "calculator"


class Settings(BaseSettings):
    NAME_SERVICE: str = NAME_SERVICE_DEFAULT

    SERVER_HOST: str = '0.0.0.0'
    SERVER_PORT: int = 7002

    DB_HOST: str = 'localhost'
    DB_PORT: str = '5432'
    DB_NAME: str = NAME_SERVICE_DEFAULT
    DB_USER: str = 'calculator'
    DB_PASSWORD: str = 'password'

    IS_DEBUG: bool

    class Config:
        env_file = "../.env"


@lru_cache
def get_settings_app() -> Settings:
    return Settings()


settings_app = get_settings_app()
