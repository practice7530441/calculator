from fastapi import APIRouter

from apps.calculator.api import router as calculator_router

router = APIRouter()
router.include_router(calculator_router)
