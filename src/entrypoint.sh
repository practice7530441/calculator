#!/bin/sh

alembic upgrade head
uvicorn --port ${SERVER_PORT} --host ${SERVER_HOST} --loop uvloop --reload main:app
