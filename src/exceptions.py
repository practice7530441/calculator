from typing import Any


class ServiceException(Exception):
    def __init__(self, code_status: int, details: Any):
        self.code_status = code_status
        self.details = details


class WrongOperatorException(ServiceException):
    def __init__(self, details: Any):
        super().__init__(
            code_status=400,
            details=details
        )


class UnknownTaskException(ServiceException):
    def __init__(self, details):
        super().__init__(
            code_status=400,
            details=details
        )
