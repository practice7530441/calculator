from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from apps.basic.schemas import Pagination
from apps.basic.utils import get_pagination
from apps.calculator.schemas import TaskIn, TaskOut, TaskResult
from apps.calculator.services import TaskService
from db.database import get_session

router = APIRouter()


@router.get(
    path='/task',
    name='Get list of tasks'
)
async def get_list_tasks(
        session: AsyncSession = Depends(get_session),
        pagination: Pagination = Depends(get_pagination),
):
    return await TaskService(session=session).list(
        pagination=pagination
    )


@router.post(
    path='/task',
    name='Create new task',
    status_code=201,
)
async def create_new_task(
        data: TaskIn,
        session: AsyncSession = Depends(get_session)
) -> TaskOut:
    return await TaskService(session=session).create(data=data)


@router.get(
    path='/task/{id_task}',
    name='Get result task',
    status_code=200
)
async def get_result_task(
        id_task: str,
        session: AsyncSession = Depends(get_session)
) -> TaskResult:
    return await TaskService(session=session).get_task(
        id_task=id_task
    )
