import asyncio
from asyncio import AbstractEventLoop, Task

from pydantic import BaseModel
from sqlalchemy.ext.asyncio.session import AsyncSession

from apps.basic.managers import ManagerBase
from apps.basic.schemas import Pagination
from apps.basic.services import BaseService
from apps.calculator.models import Tasks
from apps.calculator.schemas import SchemaTask, TaskResult
from exceptions import UnknownTaskException, WrongOperatorException


class TaskList:
    __instance = None
    feather = dict()

    def __new__(cls):
        if not cls.__instance:
            cls.__instance = super(TaskList, cls).__new__(cls)
        return cls.__instance


class TaskService(BaseService):
    model = Tasks

    def __init__(self, session: AsyncSession):
        self.session = session
        self.methods = {
            "+": self.sum,
            "-": self.sub,
            "*": self.mul,
            "/": self.div
        }
        self.manager = ManagerBase(session=self.session, model=self.model)
        self.tasks = TaskList()

    def task_callback(self, task):
        id_task = str(id(task))
        self.tasks.feather[id_task] = task.result()

    async def get_method(self, operator: str):
        if operator in self.methods:
            return self.methods[operator]
        raise WrongOperatorException(details="This operator doesn't support")

    async def get_status(self, task: Task, loop: AbstractEventLoop):
        pending = asyncio.all_tasks(loop)
        return self.STATUSES[task in pending]

    async def create(self, data: BaseModel):
        loop = asyncio.get_running_loop()
        method = await self.get_method(data.operator)
        task = loop.create_task(method(first=data.first, second=data.second))

        status = await self.get_status(task=task, loop=loop)
        id_task = str(id(task))
        task.add_done_callback(self.task_callback)

        schema = SchemaTask(id_task=id_task, status=status)
        data_create = schema.model_dump(exclude_unset=True)
        return await self.manager.create(data_create)

    async def list(self, *, pagination: Pagination = None):
        query = self.manager.select_all()

        if pagination is None:
            pagination = Pagination(size_page=-1, number_page=1)

        return await self.list_paginated(
            query=query,
            pagination=pagination
        )

    async def get_task(self, id_task: str):
        task = self.tasks.feather.get(id_task)

        loop = asyncio.get_running_loop()
        status = await self.get_status(task=task, loop=loop)

        if not status == "FINISHED":
            if not task:
                raise UnknownTaskException("Task with this id doesn't exist")

        query = self.manager.select_from_model(id_task=id_task)

        result = self.tasks.feather.pop(id_task)

        data_update = TaskResult(
            result=result,
            status=status,
            id_task=id_task
        ).model_dump(exclude_unset=True)
        instance = (await self.manager.execute(query)).scalars().first()

        updated_instance = await self.manager.update(
            instance=instance,
            data_update=data_update
        )

        return updated_instance
