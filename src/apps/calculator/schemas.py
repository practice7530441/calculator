from typing import Optional

from pydantic import BaseModel, PositiveInt


class SchemaTask(BaseModel):
    id: Optional[PositiveInt] = None
    id_task: Optional[str]
    status: Optional[str]

    class Config:
        from_attributes = True


class TaskIn(BaseModel):
    first: int
    second: int
    operator: str


class TaskOut(SchemaTask):
    pass


class TaskResult(SchemaTask):
    result: float


class TaskList(BaseModel):
    tasks: list[TaskOut]
