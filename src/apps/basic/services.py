from sqlalchemy import Executable

from apps.basic.mixins import MixinPagination


class BaseService(MixinPagination):
    model = None
    STATUSES = {
        0: "FINISHED",
        1: "IN_PENDING"
    }

    def __init_subclass__(cls, **kwargs):
        if not cls.model:
            raise NotImplementedError

    async def sum(self, first: int, second: int):
        return first + second

    async def mul(self, first: int, second: int):
        return first * second

    async def sub(self, first: int, second: int):
        return first - second

    async def div(self, first: int, second: int):
        return first / second

    async def execute(self, query: Executable):
        return await self.manager.execute(query)
