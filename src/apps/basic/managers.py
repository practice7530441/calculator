from typing import Any, Coroutine, Generator, Optional, TypeVar

from sqlalchemy import Column, Select, select, delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import declarative_base

Base = declarative_base()

ModelType = TypeVar('ModelType', bound=Base)


class ManagerBase:
    def __init__(self, session: AsyncSession, model):
        self.session = session
        self.Model = model

    def convert_to_conditions(self, conditions: dict[str, Any]) -> Generator[Column, None, None]:
        for attr, value in conditions.items():
            yield getattr(self.Model, attr) == value

    def _get_conditions(self, conditions: dict[str, Any]):
        yield from self.convert_to_conditions(conditions)

    def execute(self, query) -> Coroutine:
        return self.session.execute(query)

    async def create(self, data: Optional[dict]):
        instance = self.Model(**data)

        self.session.add(instance)
        await self.session.commit()
        await self.session.refresh(instance)

        return instance

    def select_all(self, *fields) -> Select:
        selects = fields if fields else (self.Model,)
        query = select(*selects)

        return query

    def select_from_model(self, *fields, **conditions) -> Select:
        selects = fields if fields else (self.Model,)
        query = select(*selects)
        for condition in self._get_conditions(conditions):
            query = query.where(condition)

        return query

    async def update(self, instance: ModelType, data_update: dict[str, Any]) -> ModelType:
        for field in data_update.keys():
            setattr(instance, field, data_update.get(field))

        self.session.add(instance)
        await self.session.commit()
        await self.session.refresh(instance)

        return instance

    async def clear_db(self):
        await self.execute(delete(self.Model).where(self.Model.status == "IN_PENDING"))
        await self.session.commit()
