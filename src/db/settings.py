from functools import lru_cache

from pydantic_settings import BaseSettings


class SettingsDB(BaseSettings):
    DB_HOST: str = 'localhost'
    DB_NAME: str = 'calculator'
    DB_USER: str = 'calculator'
    DB_PASSWORD: str = 'password'
    DB_PORT: int = 5432


@lru_cache
def get_settings_db() -> SettingsDB:
    return SettingsDB()


settings_db = get_settings_db()
