from pydantic import PostgresDsn
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from db.settings import settings_db

Base = declarative_base()

sqlalchemy_database_url = PostgresDsn.build(
    scheme="postgresql+asyncpg",
    username=settings_db.DB_USER,
    password=settings_db.DB_PASSWORD,
    host=settings_db.DB_HOST,
    port=settings_db.DB_PORT,
    path=f"{settings_db.DB_NAME or ''}",
)

engine = create_async_engine(str(sqlalchemy_database_url), pool_pre_ping=True)
SessionLocal = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=False) # noqa.


async def get_session() -> AsyncSession:
    async with SessionLocal() as session:
        yield session
