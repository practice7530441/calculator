import asyncio
import logging
import os
from logging.config import dictConfig

import uvicorn
from fastapi import FastAPI

from api import router
from apps.basic.managers import ManagerBase
from apps.calculator.models import Tasks
from db.database import get_session
from exceptions import ServiceException
from handlers import handle_exception_crm
from settings import settings_app

logging.config.fileConfig(
    os.path.join(os.path.dirname(__file__), 'logging.conf'),
    disable_existing_loggers=False,
)
logger = logging.getLogger(__name__)

app = FastAPI(title="Calculator")

app.include_router(router, prefix="/api")

if not settings_app.IS_DEBUG:
    app.add_exception_handler(ServiceException, handle_exception_crm)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.on_event("startup")
async def clean_shadow_instances():
    # In accordance with specific declaration of db_session we can't get AsyncSession through Depends,
    # so I've changed this logic with anext function.
    session = await anext(get_session())  # noqa
    await ManagerBase(session=session, model=Tasks).clear_db()


if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    config = uvicorn.Config(
        "main:app",
        host=settings_app.SERVER_HOST,
        port=settings_app.SERVER_PORT,
        reload=True,
        loop=loop,
    )
    app_task = uvicorn.Server(config=config)
    loop.run_until_complete(app_task.serve())
