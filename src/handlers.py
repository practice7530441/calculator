from exceptions import ServiceException
from fastapi import Request
from starlette.responses import JSONResponse


def handle_exception_crm(request: Request, exception: ServiceException):
    return JSONResponse(status_code=exception.code_status, content={
            "error": {
                "details": exception.details
            }
        }
    )
