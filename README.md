# Direct by SandrSX

![alt text](https://chart.googleapis.com/chart?cht=qr&chl=http%3A%2F%2Ft.me%2FSandrSX&chs=180x180&choe=UTF-8&chld=L|2)

# Async claculator

Service for listing async tasks and returning it after call.

---

## Project Setup

1. Clone the project and go to the directory with it:

```bash
git clone https://gitlab.com/practice7530441/calculator.git
cd Calculator
```

2. Create `.env` file (you should copy `.env.example` for now):

```bash
cp .env.example .env
```

---

## Running the Service

### Locally

1. Execute following in terminal:
```bash
docker network create "demo"
docker-compose -f docker-compose-local.yml up --build
```

### Locally in PyCharm with Attached Debugger

1. Execute following in terminal:
```bash
docker network create "demo"
docker-compose -f docker-compose-local.yml build
```
2. Setup Docker Compose remote interpreter ([_instruction_](https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html)) and run `src/main.py` in debug mode.
___

## Working with Service

- Go to _http://0.0.0.0:7002/docs_ to view all service API endpoints
- The service will be available at _http://0.0.0.0:7002_
___
## Make it easy

To use all abilities of the project, call Makefile's commands

For more information try to call:

```bash
make help
```
in bash from the root path of your project

---
## Troubleshooting

1. If while testing endpoints you get a response code 500 AND traceback contains something similar to this:
```bash
sqlalchemy.exc.ProgrammingError: (sqlalchemy.dialects.postgresql.asyncpg.ProgrammingError) <class 
'asyncpg.exceptions.UndefinedTableError'>: relation <something> does not exist
``` 

Try executing this command **in running container**: 
```bash
docker exec service_crm_profiles alembic upgrade head
```

2. If You have Mac with Apple M1 processor (ARM architecture) and encounter problems building an image from dockerfile, you need to add a line with 
libffi-dev to your dockerfile. It should look like this:
```dockerfile
RUN set -eux \
    && apk update && apk upgrade  \
    && apk add --no-cache --virtual .build-deps \
    gcc \
    musl-dev \
    build-base \
    python3-dev \
    bash \
    git \
    libffi-dev \
    && pip install --upgrade pip setuptools wheel \
    && rm -rf /root/.cache/pip \
    && rm -rf tmp
```

# Conclusion: 

I will be glad to receive any evaluation, criticism and suggestions for my code :)
